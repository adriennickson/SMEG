import TweenLite from 'gsap/TweenLite';

// require('../css/main.css');
// require('../css/grayscale.min.css');
// require('../vendor/font-awesome/css/font-awesome.min.css');
// require('../vendor/bootstrap/css/bootstrap.min.css');


require('../rs-plugin/css/settings.css');
require('../css/core-icons/core_style.css');
require('../css/icons/style.css');
require('../css/scripts.css');
require('../css/style.css');

require('../js/jquery.js');
require('../js/jquery-ui.min.js');
require('../rs-plugin/js/TweenLite.js');
require('../rs-plugin/js/jquery.themepunch.tools.min.js');
require('../rs-plugin/js/jquery.themepunch.revolution.min.js');
require('../js/prettify.js');
// require('../js/scripts.js');
//  require('../js/custom.js');

// https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenLite.min.js
// https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/CSSPlugin.min.js