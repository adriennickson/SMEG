<?php

namespace App\Controller;

use App\Entity\Service;
use App\Entity\SousService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SiteController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $blogPost =  $this->get('sonata.news.manager.post')->getPager([],1,4,[]);

        return $this->render('site/index.html.twig', [
            'page' => 0,
            'blogPost' => $blogPost->getResults()
        ]);
    }

    /**
     * @Route("/services", name="services")
     */
    public function services()
    {
        return $this->render('site/services.html.twig', [
            'page' => 1,
            // 'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/services/{id}", name="service")
     */
    public function service(Service $service)
    {
        return $this->render('site/service.html.twig', [
            'page' => 1,
            'service' => $service,
            // 'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/sousservices/{id}", name="sousservice")
     */
    public function sousservice(SousService $sousService)
    {
        return $this->render('site/service.html.twig', [
            'page' => 1,
            'sousservice' => $sousService,
        ]);
    }

    /**
     * @Route("/en-construction", name="en_construction")
     */
    public function enConstruction()
    {
        return $this->render('site/enConstruction.html.twig', [
            'page' => 99,
            // 'controller_name' => 'SiteController',
        ]);
    }

}
