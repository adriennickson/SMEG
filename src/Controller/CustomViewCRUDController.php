<?php

// src/Controller/CustomViewCRUDController.php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

class CustomViewCRUDController extends CRUDController
{
    public function listAction()
    {
        return $this->renderWithExtraParams('admin/custom_view.html.twig');
    }
}