<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\MediaBundle\Form\Type\MediaType;

class ServiceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content', ['class' => 'col-md-8'])
                ->add('nom', TextType::class)
                ->add('description', TextType::class)
                ->add('contenu', TextareaType::class, array(
                    'attr' => array('rows' => 10)
                ))
                ->add('responsable')
            ->end()
            ->with('Meta data', ['class' => 'col-md-4'])
                ->add('logo', ModelType::class, array(
                    'class' => 'App\Application\Sonata\MediaBundle\Entity\Media',
                    'property' => 'name',
                ))
                ->add('image', ModelType::class, array(
                    'class' => 'App\Application\Sonata\MediaBundle\Entity\Media',
                    'property' => 'name',
                ))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('nom');
        $datagridMapper->add('description');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('nom');
        $listMapper->add('description');
        $listMapper->add('logo');
    }
}