<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SousServiceRepository")
 */
class SousService
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \App\Application\Sonata\MediaBundle\Entity\Media
     * @ORM\OneToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    private $logo;

    /**
     * @var \App\Application\Sonata\MediaBundle\Entity\Media
     * @ORM\OneToOne(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     */
    private $image;

    /**
     * @var Service
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", cascade={}, fetch="EAGER")
     */
    private $service;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Realisation", inversedBy="sousServices")
     * @ORM\JoinTable(name="sousservice_realisation",
     *      joinColumns={@ORM\JoinColumn(name="sousservice_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="realisation_id", referencedColumnName="id")}
     * )
     */
    private $realisations;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Devis", mappedBy="sousServices")
     */
    private $examplesDeDevis;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=false)
     */
    private $contenu;

    /**
     * SousService constructor.
     */
    public function __construct()
    {
        $this->realisations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return Collection
     */
    public function getRealisations()
    {
        return $this->realisations;
    }

    /**
     * @param Collection $realisations
     */
    public function setRealisations($realisations)
    {
        $this->realisations = $realisations;
    }

    /**
     * @param Realisation $realisation
     */
    public function addRealisation($realisation)
    {
        $this->realisations->add($realisation);
    }

    /**
     * @param Realisation $realisation
     */
    public function removeRealisation($realisation)
    {
        $this->realisations->removeElement($realisation);
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param string $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }


    /**
     * Set logo
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $logo
     *
     * @return SousService
     */
    public function setLogo(\App\Application\Sonata\MediaBundle\Entity\Media $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \App\Application\Sonata\MediaBundle\Entity\Media
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set image
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return SousService
     */
    public function setImage(\App\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \App\Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add examplesDeDevi
     *
     * @param \App\Entity\Devis $examplesDeDevi
     *
     * @return SousService
     */
    public function addExamplesDeDevi(Devis $examplesDeDevi)
    {
        $this->examplesDeDevis[] = $examplesDeDevi;

        return $this;
    }

    /**
     * Remove examplesDeDevi
     *
     * @param \App\Entity\Devis $examplesDeDevi
     */
    public function removeExamplesDeDevi(Devis $examplesDeDevi)
    {
        $this->examplesDeDevis->removeElement($examplesDeDevi);
    }

    /**
     * Get examplesDeDevis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExamplesDeDevis()
    {
        return $this->examplesDeDevis;
    }
}
