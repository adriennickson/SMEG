<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RealisationRepository")
 */
class Realisation
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\SousService", mappedBy="realisations")
     */
    private $sousServices;

    /**
     * @var Devis
     * @ORM\OneToOne(targetEntity="App\Entity\Devis", inversedBy="realisation")
     */
    private $devis;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinTable(name="realisation_images",
     *      joinColumns={@ORM\JoinColumn(name="realisation_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")}
     * )
     */
    private $images;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=false)
     */
    private $contenu;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * Realisation constructor.
     */
    public function __construct()
    {
        $this->sousServices = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    // galerie
    // devis
    // commentaire
    // add your own fields


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getSousServices()
    {
        return $this->sousServices;
    }

    /**
     * @param ArrayCollection $sousServices
     */
    public function setSousServices($sousServices)
    {
        $this->sousServices = $sousServices;
    }

    /**
     * @param SousService $sousService
     */
    public function addSousService($sousService)
    {
        $this->sousServices->add($sousService);
    }

    /**
     * @param SousService $sousService
     */
    public function removeSousService($sousService)
    {
        $this->sousServices->removeElement($sousService);
    }

    /**
     * @return ArrayCollection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param ArrayCollection $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param string $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Realisation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set devis
     *
     * @param \App\Entity\Devis $devis
     *
     * @return Realisation
     */
    public function setDevis(Devis $devis = null)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * Get devis
     *
     * @return \App\Entity\Devis
     */
    public function getDevis()
    {
        return $this->devis;
    }

    /**
     * Add image
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return Realisation
     */
    public function addImage(App\Application\Sonata\MediaBundle\Entity\Media $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function removeImage(App\Application\Sonata\MediaBundle\Entity\Media $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
