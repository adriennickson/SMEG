<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DevisRepository")
 */
class Devis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LigneDeDevis", mappedBy="devis", cascade={"remove"})
     */
    private $lignesDeDevis;

    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="App\Entity\Realisation", mappedBy="devis")
     */
    private $realisation;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinTable(name="devis_images",
     *      joinColumns={@ORM\JoinColumn(name="devis_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")}
     * )
     */
    private $images;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\SousService", inversedBy="examplesDeDevis")
     * @ORM\JoinTable(name="devis_sousService")
     */
    private $sousServices;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $infosAdditonneles;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lignesDeDevis = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->sousServices = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set infosAdditonneles
     *
     * @param string $infosAdditonneles
     *
     * @return Devis
     */
    public function setInfosAdditonneles($infosAdditonneles)
    {
        $this->infosAdditonneles = $infosAdditonneles;

        return $this;
    }

    /**
     * Get infosAdditonneles
     *
     * @return string
     */
    public function getInfosAdditonneles()
    {
        return $this->infosAdditonneles;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Devis
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add lignesDeDevi
     *
     * @param \App\Entity\LigneDeDevis $lignesDeDevi
     *
     * @return Devis
     */
    public function addLignesDeDevi(LigneDeDevis $lignesDeDevi)
    {
        $this->lignesDeDevis[] = $lignesDeDevi;

        return $this;
    }

    /**
     * Remove lignesDeDevi
     *
     * @param \App\Entity\LigneDeDevis $lignesDeDevi
     */
    public function removeLignesDeDevi(LigneDeDevis $lignesDeDevi)
    {
        $this->lignesDeDevis->removeElement($lignesDeDevi);
    }

    /**
     * Get lignesDeDevis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLignesDeDevis()
    {
        return $this->lignesDeDevis;
    }

    /**
     * Set realisation
     *
     * @param \App\Entity\Realisation $realisation
     *
     * @return Devis
     */
    public function setRealisation(Realisation $realisation = null)
    {
        $this->realisation = $realisation;

        return $this;
    }

    /**
     * Get realisation
     *
     * @return \App\Entity\Realisation
     */
    public function getRealisation()
    {
        return $this->realisation;
    }

    /**
     * Add image
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return Devis
     */
    public function addImage(App\Application\Sonata\MediaBundle\Entity\Media $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $image
     */
    public function removeImage(App\Application\Sonata\MediaBundle\Entity\Media $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add sousService
     *
     * @param \App\Entity\SousService $sousService
     *
     * @return Devis
     */
    public function addSousService(SousService $sousService)
    {
        $this->sousServices[] = $sousService;

        return $this;
    }

    /**
     * Remove sousService
     *
     * @param \App\Entity\SousService $sousService
     */
    public function removeSousService(SousService $sousService)
    {
        $this->sousServices->removeElement($sousService);
    }

    /**
     * Get sousServices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSousServices()
    {
        return $this->sousServices;
    }

}
