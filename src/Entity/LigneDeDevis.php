<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LigneDeDevisRepository")
 */
class LigneDeDevis
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=100, nullable=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private $unite;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $prixUnitaire;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $pourcentageTVA;

    /**
     * @var Devis
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Devis", cascade={}, inversedBy="lignesDeDevis", fetch="LAZY")
     */
    private $devis;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return LigneDeDevis
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return LigneDeDevis
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return LigneDeDevis
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set prixUnitaire
     *
     * @param integer $prixUnitaire
     *
     * @return LigneDeDevis
     */
    public function setPrixUnitaire($prixUnitaire)
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    /**
     * Get prixUnitaire
     *
     * @return integer
     */
    public function getPrixUnitaire()
    {
        return $this->prixUnitaire;
    }

    /**
     * Set pourcentageTVA
     *
     * @param integer $pourcentageTVA
     *
     * @return LigneDeDevis
     */
    public function setPourcentageTVA($pourcentageTVA)
    {
        $this->pourcentageTVA = $pourcentageTVA;

        return $this;
    }

    /**
     * Get pourcentageTVA
     *
     * @return integer
     */
    public function getPourcentageTVA()
    {
        return $this->pourcentageTVA;
    }

    /**
     * Set devis
     *
     * @param \App\Entity\Devis $devis
     *
     * @return LigneDeDevis
     */
    public function setDevis(Devis $devis = null)
    {
        $this->devis = $devis;

        return $this;
    }

    /**
     * Get devis
     *
     * @return \App\Entity\Devis
     */
    public function getDevis()
    {
        return $this->devis;
    }
}
