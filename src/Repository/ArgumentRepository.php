<?php

namespace App\Repository;

use App\Entity\Argument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Argument|null find($id, $lockMode = null, $lockVersion = null)
 * @method Argument|null findOneBy(array $criteria, array $orderBy = null)
 * @method Argument[]    findAll()
 * @method Argument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArgumentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Argument::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
