<?php

namespace App\Application\SERTCO\MoocBundleRepository;

use App\Application\SERTCO\MoocBundle\Entity\Enseignement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Enseignement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enseignement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enseignement[]    findAll()
 * @method Enseignement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnseignementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Enseignement::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('i')
            ->where('i.something = :value')->setParameter('value', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
