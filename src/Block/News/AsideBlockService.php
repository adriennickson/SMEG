<?php

namespace App\Block\News;

use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\HttpFoundation\Response;
use Sonata\CoreBundle\Model\Metadata;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AsideBlockService extends AbstractBlockService
{
    public function getName()
    {
        return 'Aside block';
    }

    /**
     * Define valid options for a block of this type.
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'title'    => 'aside block',
            'template' => 'block/news/aside.html.twig',
        ));
    }

    /**
     * The block context knows the default settings, but they can be
     * overwritten in the call to render the block.
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {

        $block = $blockContext->getBlock();

        if (!$block->getEnabled()) {
            return new Response();
        }

        // merge settings with those of the concrete block being rendered
        $settings = $blockContext->getSettings();
        $resolver = new OptionsResolver();
        $resolver->setDefaults($settings);

        $blockContext->getBlock()->setName('aside');
        dump($response);
        return $this->renderResponse(
            $blockContext->getTemplate(),
            array(
            'feeds'     => 'un jour un jour',
            'block'     => $blockContext->getBlock(),
            'settings'  => $blockContext->getSettings()
            ),
            $response
        );
    }

    // These methods are required by the sonata block service interface.
    // They are not used in the CMF. To edit, create a symfony form or
    // a sonata admin.

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        throw new \Exception();
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        throw new \Exception();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockMetadata($code = null)
    {
        return new Metadata($this->getName(), (null !== $code ? $code : $this->getName()), false, 'SonataBlockBundle', ['class' => 'fa fa-file']);
    }
}