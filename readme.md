<body>
    <h2>
        Website for companies.
    <h2>
    <p>
    </p>
    <p>
        Website developed in symfony flex. Offering a company (SMEG in this case) the following modules:
    </p>
    <ul>
        <li>Presentation of his services</li>
        <li>Presentation of associated sub services</li>
        <li>Presentation of the arguments that make the company the best</li>
        <li>blogging engine</li>
        <li>Taxonomy management</li>
        <li>Media management (images, videos and file)</li>
        <li>Quotation template management</li>
        <li>Management of client portfolios</li>
        <li>Project follow-up</li>
        <li>Online formation</li>
    </ul>
    <p></p>
    <h3>
        Symfony bundle use:
    </h3>
    <ul>
        <li>SonataAdminBundle</li>
        <li>SonataBlockBundle</li>
        <li>SonataMediaBundle</li>
        <li>SonataNewsBundle</li>
        <li>FOSUserBundle</li>
        <li>ApiPlatformBundle</li>
    </ul>
    <p></p>
    <h3>
        Features already available:
    </h3>
    <ul>
        <li>Presentation of his services</li>
        <li>Presentation of associated sub services</li>
        <li>Presentation of the arguments that make the company the best</li>
        <li>blogging engine </li>
        <li>Taxonomy management</li>
        <li>Media Management</li>
    </ul>
    <p></p>
    <h3>
        Template used for the front office:
    </h3>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="https://themeforest.net/item/vozx-multipurpose-business-html5-template/13479194">
            Vozx - Multipurpose Business HTML5 Template
        </a>
    </p>
    <p></p>
</body>
